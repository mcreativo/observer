<?php


class ClassA implements Observed
{
    /**
     * @var Observer[]
     */
    protected $observers = [];


    public function changeState()
    {
        echo 'ClassA change state' . PHP_EOL;
        $this->notify();
    }


    /**
     * @param Observer $observer
     * @return ClassA
     */
    public function addObserver(Observer $observer)
    {
        $this->observers[] = $observer;
        return $this;
    }

    /**
     * @param Observer $observer
     * @return ClassA
     */
    public function removeObserver(Observer $observer)
    {
        if (isset($this->observers[array_search($observer, $this->observers)]))
            unset($this->observers[array_search($observer, $this->observers)]);
        return $this;
    }

    protected function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->changedStateOfClassA();
        }
    }


}