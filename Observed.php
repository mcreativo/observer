<?php


interface Observed
{
    public function addObserver(Observer $observer);

    public function removeObserver(Observer $observer);

} 