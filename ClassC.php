<?php


class ClassC implements Observer
{

    public function changedStateOfClassA()
    {
        echo 'ClassC: ClassA changed state' . PHP_EOL;
    }
}