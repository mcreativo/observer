<?php


$classA = new ClassA();
$classB = new ClassB();
$classC = new ClassC();

$classA->addObserver($classB);
$classA->addObserver($classC);


$classA->changeState();
