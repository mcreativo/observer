<?php


class ClassB implements Observer
{

    public function changedStateOfClassA()
    {
        echo 'ClassB: ClassA changed state' . PHP_EOL;
    }
}